﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{

    [SerializeField]
    public List<Block> ListBlock;

    void Start()
    {
        ListBlock = new List<Block>();

    }


    public bool IsHole()
    {
        bool ishole = false;
        foreach (Block bl in ListBlock)
        {

            if (bl.hp < 0)
            {
                ishole = true;
                break;
            }

            else
            {
                ishole = false;
            }
        }
        return ishole;
    }


    public void DangerousCondition()
    {
       
    }

    public void Update()
    {
        if (IsHole())
        {
            DangerousCondition();
        }
       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class GravitySystem : Block
{
    [SerializeField] private float playerSpeedNoGravity = 1;
    [SerializeField] private float playerRunSpeedNoGravity = 2;

    private float playerSpeedStart;
    private float playerRunSpeedStart;

    private FirstPersonController player;

    protected override void Start()
    {
        base.Start();
        player = GameObject.FindWithTag("Player").GetComponent<FirstPersonController>();
        playerSpeedStart = player.m_WalkSpeed;
        playerRunSpeedStart = player.m_RunSpeed;
    }

    public override void Assemble()
    {
        base.Assemble();

        player.m_WalkSpeed = playerSpeedStart;
        player.m_RunSpeed = playerRunSpeedStart;
    }

    public override void Disasemble()
    {
        base.Disasemble();

        player.m_RunSpeed = playerRunSpeedNoGravity;
        player.m_WalkSpeed = playerSpeedNoGravity;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BlackBoard : MonoBehaviour
{
    [SerializeField] private Block[] ventelations;
    [SerializeField] private Block[] hulls;
    [SerializeField] private Block[] cargo;
    
    [SerializeField] private Block hydroponics;
    [SerializeField] private Block hyperDrive;
    [SerializeField] public float timeToEscape;
    [SerializeField] public float timeToEscapeNoDrive;
    [SerializeField] private Block[] shield;
    [SerializeField] private TextMesh hyperdriveCounter;
    [SerializeField] private Block stelthSystem;
    [SerializeField] private Block gravityModule;
    
    [SerializeField] private float aditionalEnemySpawnDelay;
    [SerializeField] private Transform ship;
    [SerializeField] float speed = 10.0f; //how fast it shakes
    [SerializeField] float amount = .1f; //how much it shakes
    [SerializeField] private float timeToShake = 10;
    [SerializeField] private float chancePerShield = 0.1f;
    [SerializeField] private float oxygenStep;
    [SerializeField] private float oxygenOutStep;
    [SerializeField] private Block scaner;
    private Vector3 shipPosition;

    private int pointsPerBox = 11;
   
    private float oxygenLevel;
    private bool shake = false;
    private float time;
    private bool hullDamage = false;

    public UI.State state;

    public bool HullDamage
    {
        get { return hullDamage && !scaner.Disasembled; }
    }

    public float OxygenLevel
    {
        get { return oxygenLevel; }
    }

    public bool StelthOnline
    {
        get { return !stelthSystem.Disasembled; }
    }

    public float ShieldsPower
    {
        get { return shield.Count(s => !s.Disasembled) * chancePerShield; }
    }

    public float RemaningTime
    {
        get { return TimeToEscape - Time.timeSinceLevelLoad; }
    }

    public float TimeToEscape
    {
        get { return hyperDrive.Disasembled ? timeToEscapeNoDrive : timeToEscape; }
    }

    public float StealthTime
    {
        get { return StelthOnline ? aditionalEnemySpawnDelay : 0; }
    }

    public int GetScore
    {
        get { return cargo.Count(c => !c.Disasembled) * pointsPerBox; }
    }

    public bool GravityOn
    {
        get { return !gravityModule.Disasembled; }
    }

    public Vector3 ClosestHole(Vector3 pos)
    {
        if (!HullDamage)
            return Vector3.zero;

        Block first = null;
        foreach (var h in hulls)
        {
            if (h.Disasembled)
            {
                first = h;
                break;
            }
        }

        if(first == null)
            return Vector3.zero;
        

        return first.transform.position;
    }

    // Use this for initialization
    void Start ()
	{
	    //oxygenStep =  Time.deltaTime*OxygenRecoveryTime;
	    shipPosition = ship.position;

        foreach (var block in hulls)
	    {
	        block.chanceToDismissAttack = ShieldsPower;
            block.onHit += OnHit;
        }

	    foreach (var block in shield)
	    {
	        block.onDisasemble += OnShieldDisasemble;
	    }
	}

    private void OnShieldDisasemble()
    {
        foreach (var block in hulls)
        {
            block.chanceToDismissAttack = ShieldsPower;            
        }
    }

    private void OnHit()
    {
        shake = true;
        time = Time.timeSinceLevelLoad;
    }

    // Update is called once per frame
	void Update () {

	    if (oxygenLevel != 1 && !hydroponics.Disasembled)
	    {
	        var step = (float)ventelations.Count(v => !v.Disasembled) / (float)ventelations.Length;

	        oxygenLevel += step * oxygenStep * Time.deltaTime;

	        oxygenLevel = Mathf.Clamp(oxygenLevel, 0, 1);
	    }

	    var count = hulls.Count(h => h.Disasembled);

        hullDamage = count > 0;

	    hyperdriveCounter.text = Mathf.CeilToInt(RemaningTime).ToString();

	    if (hullDamage)
	    {
	        oxygenLevel -= (oxygenStep + oxygenOutStep* Mathf.Sqrt(count)) * Time.deltaTime;
	    }

	    if (shake)
	    {
	        ship.position += new Vector3(Random.Range(-1f,1f) * amount, Random.Range(-1f, 1f) * amount,
	            Random.Range(-1f, 1f) * amount);


	        if (Time.timeSinceLevelLoad - time >= timeToShake)
	        {
	            ship.position = shipPosition;
	            shake = false;
	        }
	    }

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjDisabler : Block
{
    [SerializeField]
    private GameObject[] objToDisable;

    public override void Assemble()
    {
        base.Assemble();

        foreach (var o in objToDisable)
        {
            o.gameObject.SetActive(true);
        }
    }

    public override void Disasemble()
    {
        base.Disasemble();

        foreach (var o in objToDisable)
        {
            o.gameObject.SetActive(false);
        }
    }
}

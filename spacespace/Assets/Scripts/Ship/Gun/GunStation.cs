﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class GunStation : MonoBehaviour
{
    public GameObject gunCamera;
    public Camera camera;
    public Rigidbody bullet;
    
    public float force = 50;
    private Player player;
    [SerializeField] private TextMesh text;
    [SerializeField] private Transform muzle1;
    [SerializeField] private Transform muzle2;
    [SerializeField] private Animator animator;
    [SerializeField] ParticleSystem particleSystem1;
    [SerializeField] ParticleSystem particleSystem2;
    [SerializeField] private GameObject arrow;
    private bool playerIsInside;

    //private int bulletsAmount;
	// Use this for initialization
	void Awake ()
	{
	    player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
	    gunCamera.GetComponent<FirstPersonController>().m_Camera = camera; //kostyl :(
        gunCamera.SetActive(false);
    }

    public void GetInside()
    {
        player.Disable();
        playerIsInside = true;
        gunCamera.SetActive(true);
        text.text = player.currentBlocks.ToString();
    }

    void Update()
    {
        if (Input.GetKeyDown("e"))
        {
            GetOut();
        }

        if (playerIsInside)
        {
            if (Input.GetMouseButtonDown(0))
            {
                    
                Fire();
               

            }
        }
    }

    public void Higlight(bool b)
    {
        arrow.SetActive(b);
    }

    private void Fire()
    {
        if (player.currentBlocks > 0)
        {
            if (particleSystem1!= null&& particleSystem2!=null)
            {
                   particleSystem1.Play();
                   particleSystem2.Play();
                
            }         
               

               

            var b = GameObject.Instantiate(bullet, muzle1.position, Quaternion.identity);
            var b2 = GameObject.Instantiate(bullet, muzle2.position, Quaternion.identity);
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            b.AddForce(Vector3.up * 2300 + ray.direction * force);
            b2.AddForce(Vector3.up * 2300 + ray.direction * force);
            player.currentBlocks--;
            text.text = player.currentBlocks.ToString();
            animator.SetTrigger("fire");
        }
    }

    private void GetOut()
    {
        if (particleSystem1 != null && particleSystem2 != null)
        {
                    particleSystem1.Stop();
                    particleSystem2.Stop();

        }

        gunCamera.SetActive(false);
        playerIsInside = false;
        player.SetActive();
    }
}

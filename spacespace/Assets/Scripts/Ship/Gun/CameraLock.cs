﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class CameraLock : MonoBehaviour
{
    [SerializeField] private float minRotationX;
    [SerializeField] private float maxRotationX;
    [SerializeField] private float minRotationY;
    [SerializeField] private float maxRotationY;
    
    private float mouseSpeed = 25;

    float rotationX = 0;
    float rotationY = 0;

    private float initialRotation;
    // Use this for initialization
    void Start ()
    {
        initialRotation = transform.rotation.eulerAngles.y;
    }

    void Update()
    {
        rotationX -= CrossPlatformInputManager.GetAxis("Mouse Y") * Time.deltaTime * mouseSpeed;
        rotationX = Mathf.Clamp(rotationX, minRotationX, maxRotationX);
        rotationY += CrossPlatformInputManager.GetAxis("Mouse X") * Time.deltaTime * mouseSpeed;
        rotationY = Mathf.Clamp(rotationY, minRotationY, maxRotationY);

        transform.rotation = Quaternion.Euler(rotationX, rotationY + initialRotation, 0);
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

public class Block : MonoBehaviour
{
    [SerializeField] private GameObject wall;
    [SerializeField] public int blocks = 1;
    [SerializeField] private bool permamentDisable = false;
    private Material[] wallMaterial;
    private Collider wallCollider;
    public float chanceToDismissAttack = 0;
    private Dictionary<Material, Color> colorMap = new Dictionary<Material, Color>();

    public bool hasInfo;
    public string info;
    public Sprite sprite;

    public int hp = 100;

    private bool disasembled = false;
    public Action onHit;
    public Action onDisasemble;

    protected virtual void Start()
    {
        wallMaterial = wall.GetComponent<Renderer>().materials;

        foreach (var material in wallMaterial)
        {
            //material.shader = Shader.Find("Transparent/Cutout/Diffuse");
            //ChangeRenderMode(material, BlendMode.Transparent);
            colorMap.Add(material,material.color);
            material.enableInstancing = true;
            //ChangeRenderMode(material, BlendMode.Opaque);

        }

        wallCollider = wall.GetComponent<Collider>();
    }

    public bool Disasembled
    {
        get { return disasembled; }
    }

    public virtual void Disasemble()
    {
        foreach (var material in wallMaterial)
        {
            //material.shader = Shader.Find("Transparent/Cutout/Diffuse");
            ChangeRenderMode(material, BlendMode.Transparent);
        }
        disasembled = true;
        wall.SetActive(false);

        if (onDisasemble != null)
        {
            onDisasemble.Invoke();
        }

        if(permamentDisable)
            GameObject.Destroy(gameObject);
    }

    public virtual void Assemble()
    {
        foreach (var material in wallMaterial)
        {
            //material.shader = Shader.Find("Transparent/Cutout/Diffuse");
            if(material.GetInt("_DstBlend") == (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha)
                ChangeRenderMode(material, BlendMode.Transparent);
            else
                ChangeRenderMode(material, BlendMode.Opaque);

        }
        disasembled = false;
        wall.SetActive(true);
    }

    public void TakeDamage(int damage)
    {
        if (Random.value <= chanceToDismissAttack)
            return;

        hp -= damage;

        if(onHit != null)
            onHit.Invoke();

        if (hp <= 0)
            Disasemble();
    }

    public void Highlith()
    {

        foreach (var material in wallMaterial)
        {
            var c = material.color;
            c.a = 0.35f;
            c.g = 1;
            material.color = c;
        }
       

        wall.SetActive(true);
        wallCollider.enabled = false;
        
    }

    public void StopHiglith()
    {
        foreach (var material in wallMaterial)
        {
            var c = material.color;
            c.a = colorMap[material].a;
            c.g = colorMap[material].g;
            material.color = c;
        }

        wall.SetActive(!disasembled);
        wallCollider.enabled = true;

    }

    public enum BlendMode
    {
        Opaque,
        Cutout,
        Fade,
        Transparent
    }

    public static void ChangeRenderMode(Material standardShaderMaterial, BlendMode blendMode)
    {
        switch (blendMode)
        {
            case BlendMode.Opaque:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                standardShaderMaterial.SetInt("_ZWrite", 1);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = -1;
                break;
            case BlendMode.Cutout:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                standardShaderMaterial.SetInt("_ZWrite", 1);
                standardShaderMaterial.EnableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 2450;
                break;
            case BlendMode.Fade:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                standardShaderMaterial.SetInt("_ZWrite", 0);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.EnableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 3000;
                break;
            case BlendMode.Transparent:
                standardShaderMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                standardShaderMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                standardShaderMaterial.SetInt("_ZWrite", 0);
                standardShaderMaterial.DisableKeyword("_ALPHATEST_ON");
                standardShaderMaterial.DisableKeyword("_ALPHABLEND_ON");
                standardShaderMaterial.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                standardShaderMaterial.renderQueue = 3000;
                break;
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestruct : MonoBehaviour
{
    [SerializeField] private float timeToLive = 20;
	// Use this for initialization
	void Start ()
	{
	    StartCoroutine(DestroyInTime(timeToLive));
	}

    private IEnumerator DestroyInTime(float y)
    {
        yield return new WaitForSeconds(y);
        Destroy(gameObject);
    }

    // Update is called once per frame
	void Update () {
		
	}
}

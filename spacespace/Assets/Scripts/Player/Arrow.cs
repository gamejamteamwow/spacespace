﻿using UnityEngine;

public class Arrow : MonoBehaviour {
    [SerializeField] BlackBoard blackBoard;

    private Renderer renderer;
	// Use this for initialization
	void Start ()
	{
	    renderer = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update ()
	{

	    renderer.enabled = blackBoard.HullDamage;
	    transform.LookAt(blackBoard.ClosestHole(transform.position) + Vector3.up * 2);
	}
}

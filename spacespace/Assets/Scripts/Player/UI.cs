﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private Image[] assembleProgressImage;
    [SerializeField] private Text hullDamageText;
    [SerializeField] private BlackBoard bladBoard;
    [SerializeField] private GameObject infoBlock;
    [SerializeField] private Text info;
    [SerializeField] private Text textScore;
    [SerializeField] private Image infoIcon;
    [SerializeField] private Camera uiCamera;
    [SerializeField] private GameObject StartScreen;
    [SerializeField] private GameObject DieScreen;
    [SerializeField] private GameObject winScreen;

    public enum State
    {
        Start,
        Play,
        Die
    }

    public State state = State.Start;

    private float step;

    void Start()
    {
        step = 1f / assembleProgressImage.Length;
        StartScreen.SetActive(true);
        player.gameObject.SetActive(false);
        uiCamera.gameObject.SetActive(true);
        SetCursorLock(false);
    }

    public void Hard()
    {
        bladBoard.timeToEscape = 900;
        bladBoard.timeToEscapeNoDrive = 990;
        state = State.Play;
        bladBoard.state = State.Play;
        play();
    }

    public void Easy()
    {
        bladBoard.timeToEscape = 300;
        bladBoard.timeToEscapeNoDrive = 330;
        bladBoard.state = State.Play;
        state = State.Play;
        play();
    }

    public void Normal()
    {
        bladBoard.timeToEscape = 600;
        bladBoard.timeToEscapeNoDrive = 660;
        bladBoard.state = State.Play;
        state = State.Play;
        play();
    }

    private void play()
    {
        player.gameObject.SetActive(true);
        uiCamera.gameObject.SetActive(false);
        SetCursorLock(true);
        StartScreen.SetActive(false);
    }

    private void die()
    {
        player.gameObject.SetActive(false);
        uiCamera.gameObject.SetActive(true);
        DieScreen.gameObject.SetActive(true);
        state = State.Die;
        SetCursorLock(false);

   
    }
    private void win()
    {
        player.gameObject.SetActive(false);
        uiCamera.gameObject.SetActive(true);
        winScreen.gameObject.SetActive(true);
        textScore.text = "You've scored: " + bladBoard.GetScore.ToString();
        state = State.Die;
        SetCursorLock(false);

   
    }

    public void SetCursorLock(bool value)
    {
        
        if (!value)
        {//we force unlock the cursor if the user disable the cursor locking helper
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    void Update()
    {
        //if (player.AssembleProgress > 0)

        switch (state)
        {
            case State.Start:
            //    player.gameObject.SetActive(false);
             //   uiCamera.gameObject.SetActive(true);
              //  SetCursorLock(false);
                break;
            case State.Play:
              //  player.gameObject.SetActive(true);
              //  uiCamera.gameObject.SetActive(false);
             //   SetCursorLock(true);
             //   StartScreen.SetActive(false);

                if (bladBoard.OxygenLevel < 0 || player.transform.position.y < -10)
                {
                    die();
                }

                if (bladBoard.RemaningTime < 0)
                {
                    win();
                }

                int curProgress = Mathf.CeilToInt(player.AssembleProgress / step);
                curProgress = Mathf.Clamp(curProgress, 0, assembleProgressImage.Length);

                for (int i = 0; i < curProgress; i++)
                {
                    assembleProgressImage[i].enabled = true;
                }

                for (int i = curProgress; i < assembleProgressImage.Length; i++)
                {
                    assembleProgressImage[i].enabled = false;
                }


                infoBlock.SetActive(player.hasInfo);
                info.text = player.info;
                if (player.sprite != null)
                    infoIcon.sprite = player.sprite;

                hullDamageText.gameObject.SetActive(bladBoard.HullDamage);
                break;
            case State.Die:
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                }

                return;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
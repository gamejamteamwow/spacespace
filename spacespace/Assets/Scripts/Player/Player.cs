﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : MonoBehaviour
{
    private float assembleStep = 2f;

    public int currentBlocks = 0;

    private float assembleProgress = 0;

    private bool holdingAssemble, holdingDisasemble;

    [SerializeField] private Animator assembler;
    [SerializeField] private TextMesh text;

    [SerializeField] private Transform oMark1;
    [SerializeField] private Transform oMark2;
    [SerializeField] private Transform oMark3;
    [SerializeField] private float offGravityWalk;
    [SerializeField] private float offGravityRun;
    [SerializeField] private BlackBoard blackboard;
    
    private FirstPersonController fps;
    public bool hasInfo;
    public string info;
    public Sprite sprite;
    private float startSpeed;
    private float StartRunSpeed;
    public float AssembleProgress
    {
        get { return assembleProgress; }
    }

    public bool locMouse = true;

  /*  public int BulletsCount
    {
        get { return currentBlocks; }
    }*/

    private Block higlitedWall;

    void Awake()
    {
        fps = GetComponent<FirstPersonController>();
        startSpeed = fps.m_WalkSpeed;
        StartRunSpeed = fps.m_RunSpeed;
    }

    void setSpeed(bool g)
    {
        if (g)
        {
            fps.m_RunSpeed = StartRunSpeed;
            fps.m_WalkSpeed = startSpeed;
        }
        else
        {
            fps.m_RunSpeed = offGravityRun;
            fps.m_WalkSpeed = offGravityWalk;
        }
        
    }

    private void HandleWall(Block wall)
    {

        if (wall != null)
        {
            hasInfo = wall.hasInfo;
            info = wall.info;
            sprite = wall.sprite;

        }

        if (Input.GetMouseButtonDown(1))
        {
            if (currentBlocks <= 0 || !wall.Disasembled)
                return;

            holdingAssemble = true;
        }

        if (holdingAssemble && Input.GetMouseButton(1))
        {
            assembleProgress += Time.deltaTime * assembleStep;

        }
        else
        {
            holdingAssemble = false;
        
            if(holdingAssemble)
                assembleProgress = 0;

        }

        if (Input.GetMouseButtonDown(0))
        {
            holdingDisasemble = true;
        }

        if (holdingDisasemble && Input.GetMouseButton(0))
        {
            assembleProgress += Time.deltaTime * assembleStep;

        }
        else
        {
            if (holdingDisasemble)
                assembleProgress = 0;

            holdingDisasemble = false;
            
        }

        if (holdingAssemble && assembleProgress >= 1)
        {
            Repair(wall);
            holdingAssemble = false;
            holdingDisasemble = false;
            assembleProgress = 0;
        }

        if (holdingDisasemble && assembleProgress >= 1)
        {
            DissasembleWall(wall);
            holdingAssemble = false;
            holdingDisasemble = false;
            assembleProgress = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Block wall = null;
        text.text = currentBlocks.ToString();
        assembler.SetBool("assebling",assembleProgress != 0);
        setSpeed(blackboard.GravityOn);
        oMark1.gameObject.SetActive(blackboard.OxygenLevel > 0f);
        oMark2.gameObject.SetActive(blackboard.OxygenLevel > 0.33f);
        oMark3.gameObject.SetActive(blackboard.OxygenLevel > 0.66f);

        if (Input.GetKeyDown("e"))
        {
            if (Physics.Raycast(ray, out hit))
            {
                var g = hit.transform.GetComponent<GunStation>();

                if (g != null)
                {
                    g.GetInside();
                    return;
                }
            }
        }

        if (Physics.Raycast(ray, out hit,20, LayerMask.GetMask("Wall")))
        {
            wall = hit.transform.GetComponent<Block>();

            if (wall == null)
            {
                hasInfo = false;
            }

            if (wall != null && wall.Disasembled)
            {
                wall.Highlith();

                if(higlitedWall != null && higlitedWall != wall)
                    higlitedWall.StopHiglith();
                
                higlitedWall = wall;
            }
            else
            {
                if (higlitedWall != null && higlitedWall != wall)
                    higlitedWall.StopHiglith();
            }
        }
        else
        {
            holdingAssemble = false;
            hasInfo = false;
            holdingDisasemble = false;
            assembleProgress = 0;

            if(higlitedWall != null)
                higlitedWall.StopHiglith();

            higlitedWall = null;
        }

        HandleWall(wall);

       
    }

    


    private void DissasembleWall(Block wall)
    {
        currentBlocks+= wall.blocks;
        wall.Disasemble();
    }

    private void Repair(Block wall)
    {
        if (currentBlocks < wall.blocks)
            return;

        wall.Assemble();
        higlitedWall.StopHiglith();
        higlitedWall = null;
        currentBlocks-= wall.blocks;
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }

    public void SetActive()
    {
        gameObject.SetActive(true);
    }
}
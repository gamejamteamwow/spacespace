﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    [SerializeField] private int damage = 20;
    [SerializeField] AudioClip StartMove;
    [SerializeField] AudioClip FinishMove;
    [SerializeField] AudioSource audioSource;
    [SerializeField] GameObject ParticleSystem;

    // Use this for initialization
    void Start()
    {

    }
    public void Awake()
    {
        if(audioSource!= null&& StartMove != null)
        {
            audioSource.clip = StartMove;
            audioSource.Play();

        }
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if(audioSource!=null && FinishMove != null)
        {
            //audioSource.clip = FinishMove;
            //audioSource.Play();
            if(ParticleSystem!= null)
            {
                var ps = Instantiate(ParticleSystem, gameObject.transform.position, Quaternion.identity);
                ps.GetComponent<AudioSource>().clip = FinishMove;
                ps.GetComponent<AudioSource>().Play();
            }
            

        }
        
        var p = collision.transform.parent;

        if(p != null)
        {

            var e = p.GetComponent<Block>();

            if (e != null)
            {
                e.TakeDamage(damage);
            }

        }
        GameObject.Destroy(gameObject);
    }
}

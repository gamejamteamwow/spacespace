﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] public List<Enemy> EnemyList;
    [SerializeField] private Transform[] SpawnPoints;
    [SerializeField] private float distanceToSpawn = 50;
    [SerializeField] Enemy[] EnemyPrefab;
    [SerializeField] private float timeToSpawn = 10;
    [SerializeField] private GameObject player;
    [SerializeField] private BlackBoard blackBoard;

    void Start()
    {
        EnemyList = new List<Enemy>();
        StartCoroutine(TimeToSpawn());
        //blackBoard = GameObject.FindWithTag("bb").GetComponent<BlackBoard>();
    }

    public void EnemySpawn()
    {
        
        var p = Random.Range(-distanceToSpawn,distanceToSpawn);

        var sp = SpawnPoints[Random.Range(0, SpawnPoints.Length)];

        Enemy newEnemy = Instantiate(EnemyPrefab[Random.Range(0,EnemyPrefab.Length)], sp.position + sp.forward * p, Quaternion.identity);
        newEnemy.player = player.transform;
        newEnemy.transform.parent = gameObject.transform;
        newEnemy.transform.LookAt(player.transform.position);

        EnemyList.Add(newEnemy);
    }

    IEnumerator TimeToSpawn()
    {
        while (true)
        {
            if(blackBoard.state == UI.State.Play)
                EnemySpawn();

            yield return new WaitForSeconds(timeToSpawn + blackBoard.StealthTime);
        }

    }

}

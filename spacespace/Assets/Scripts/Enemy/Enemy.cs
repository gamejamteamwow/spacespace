﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class Enemy : MonoBehaviour
{

    [SerializeField]
    public int HP;

    [SerializeField]
    public float RotateSpeed = 1;

    [SerializeField]
    public float offsetRadius = 5;

    [SerializeField] private float shootingFrequency = 5;
    [SerializeField] private float decoy = 5;
    [SerializeField] private float shootingPower = 5000;
    [SerializeField] private Rigidbody enemybullet;
    [SerializeField] private Transform muzle;
    [SerializeField] GameObject particleSystem;
    [SerializeField] AudioClip DieAudioClip;
    [SerializeField] AudioClip hit;
    [SerializeField] AudioSource audioSource;
 

    private Vector3 _centre;
    private float _angle;

    public Transform player;

    private GunStation gun;

    private void Start()
    {
        var guns = GameObject.FindGameObjectsWithTag("gun").OrderBy(g => (transform.position - g.transform.position).sqrMagnitude).ToList();
        gun = guns[0].GetComponent<GunStation>();
        gun.Higlight(true);
        StartCoroutine(TimeToFire());
        _centre = transform.position;
    }

    private void Update()
    {
        _angle += RotateSpeed * Time.deltaTime;

        var offset = new Vector3(Mathf.Sin(_angle),0, Mathf.Cos(_angle)) * offsetRadius;
        gun.Higlight(true);
        transform.position = _centre + offset;
    }

    public void Die()
    {
        gun.Higlight(false);

        if (particleSystem != null&&audioSource!=null&&DieAudioClip!=null)
        {
           // audioSource.clip = DieAudioClip;
           // audioSource.Play();        
            var ps = Instantiate(particleSystem,gameObject.transform.position, Quaternion.identity);
            ps.GetComponent<AudioSource>().clip = DieAudioClip;
            ps.GetComponent<AudioSource>().Play();


        }
        Destroy(gameObject);
    }

    public void Damage(int damage)
    {
        HP = HP - damage;
        audioSource.clip = hit;
        audioSource.Play();
        if (HP < 0)
        {

            Die();
        }
    }

    public void Fire()
    {
        var pointToShoot = player.position + UnityEngine.Random.insideUnitSphere * decoy + Vector3.up * 2f;

        var velocty = pointToShoot - transform.position;
        velocty = velocty.normalized * shootingPower;

        var eb = GameObject.Instantiate(enemybullet, muzle.position, Quaternion.identity);

        eb.AddForce(velocty);
    }

    public IEnumerator TimeToFire()
    {
        while (true)
        {
            Fire();
            yield return new WaitForSeconds(shootingFrequency);
        }
    }
}
